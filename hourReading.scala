import java.text.{SimpleDateFormat, ParseException}
import java.util.Date
import scala.io.Source

val regular = new SimpleDateFormat("HH:mm:ss")
val hourMinute = new SimpleDateFormat("HH:mm")
val hour = new SimpleDateFormat("HH")


def readFile(fileName:String)=Source.fromFile(fileName).getLines.toList

val wakeHours = readFile("wakeuphour.txt")
val countries = readFile("countries.txt")
val emails = readFile("emails.txt")
val exercise = readFile("exercise.txt")
val timeToBed = readFile("timeToBed.txt")
val workArrive = readFile("workArrive.txt")
val workDayHour = readFile("workdayHour.txt")
val downTime = readFile("downTime.txt")

def tryParse(string:String):Option[Date]={
  try{
    Some(regular.parse(string))
  }
  catch {
    case e:ParseException =>
      try{
	Some(hourMinute.parse(string))
      }
      catch{
	case e:ParseException => {
	  try{
	    Some(hour.parse(string))
	  }
	  catch{
	    case i => None
	  }
	}
      }
  }
}



def extractTimeHours(hours:List[String],isNight:Boolean=false) ={
  val correctParsedLines = hours.map(tryParse(_)).flatten.filter(t => (isNight || t.getDay == 4) && t.getMonth == 0).map{d => 
    if (isNight && (d.getTime < 60L*60L*1000L*6 || d.getTime ==  60L*60L*1000L*11)){
      new Date(d.getTime + (60L *60L * 1000L * 24))
    }
    else{
      d   
    }
  }

//  correctParsedLines.foreach(println)
  // Average
  val epochTimes = correctParsedLines.map(_.getTime)
  val average = epochTimes.sum/epochTimes.size

  //(Average, %of correctness)
  (new Date(average),(epochTimes.size+0.0)/hours.size)
}

import scala.util.regexp._
val TwoDigits = """.*(\d\d)\.(\d?).*""".r
val Digit = """.*(\d)\.(\d?).*""".r
val SimpleDigit = """(\d\d).*""".r
val SimpleOneDigit = """(\d).*""".r
val Minutes = """(\d+).*(minutes|min|mins|m).*""".r


def extractHours(hours:List[String],pred:Double=>Boolean=_=>true)={
  val resultingHours = hours.map{i => i match{
    case s@Minutes(a,_) => Some((a.toInt+0.0)/60)
    case TwoDigits(a,b) => Some(a.toInt + 0.1*(if (b.isEmpty) 0 else b.toInt))
    case Digit(a,b) => Some(a.toInt + 0.1*(if (b.isEmpty) 0 else b.toInt))
    case SimpleDigit(a) => Some(a.toInt+0.0)
    case SimpleOneDigit(a) => Some(a.toInt+0.0)
    case i => None
  }
  }.flatten.filter(pred)

  //(Average,%of correct values)
  (resultingHours.sum/resultingHours.size,(resultingHours.size+0.0)/hours.size)
}

// WAKE HOURS
extractTimeHours(wakeHours)
//=> res0: (java.util.Date, Double) = (Thu Jan 01 07:50:40 WET 1970,0.9758269720101781)

// WORK ARRIVE
extractTimeHours(workArrive)
//=> res1: (java.util.Date, Double) = (Thu Jan 01 09:06:09 WET 1970,0.9697837150127226)

// WORK DAY HOURS
extractHours(workDayHour)
//=> res2: (Double, Double) = (8.573676909944515,0.9936386768447837)

// EXERCISE
extractTimeHours(exercise)
//=> res3: (java.util.Date, Double) = (Thu Jan 01 12:04:43 WET 1970,0.44737678855325913)

// EMAILS
extractHours(emails,_ <= 12)
// res4: (Double, Double) = (0.9387210473313192,0.9475190839694656)

// TIME TO BED
extractTimeHours(timeToBed,true)
// res5: (java.util.Date, Double) = (Thu Jan 01 22:54:29 WET 1970,0.9602417302798982)

// DOWN TIME
extractHours(downTime)



